package com.example.joke.controllers;

import com.example.joke.services.JokeServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class jokeController {
    private JokeServiceImp jokeServiceImp;

    @Autowired
    public jokeController(JokeServiceImp jokeServiceImp) {
        this.jokeServiceImp = jokeServiceImp;
    }
    @RequestMapping({"/",""})
    public String showJokes(Model model){
        model.addAttribute("joke",jokeServiceImp.getJokes());
        return "ChuckNorris";
    }
}
